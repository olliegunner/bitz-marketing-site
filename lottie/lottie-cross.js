var animation = bodymovin.loadAnimation({
    container: document.getElementById('lot-container-orange-1'),
    renderer: 'svg',
    loop: false,
    autoplay: false,
    prerender: true,
    path: 'img/lottie-img/plus.json'
});

cross = document.getElementById('lot-container-orange-1');

cross.addEventListener("mouseenter", fEnter);
cross.addEventListener("mouseleave", fLeave);

// Behaviour - THIS MAKES ALL ANIMATIONS PLAY BACKWARDS

function fEnter(){
    animation.removeEventListener("mouseenter", fEnter);
    animation.play();
    animation.setSpeed(1);
    animation.addEventListener("mouseleave", fLeave);
}

function fLeave(){
    animation.removeEventListener("mouseleave", fLeave);
    animation.setSpeed(-1);
    animation.play();

}







/*
btn.addEventListener('mouseenter', function() {
    animation.play();
    animation.setSpeed(1);
});

btn.addEventListener('mouseleave', function() {
    animation.setSpeed(-1);
    animation.play();
});
*/