var animation = bodymovin.loadAnimation({
    container: document.getElementById('lot-contact-container'),
    renderer: 'svg',
    loop: true,
    autoplay: true,
    prerender: false,
    path: 'img/lottie-img/lottie-wind/Wind.json'
})
