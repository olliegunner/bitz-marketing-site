// When the user scrolls the page, execute myFunction
window.onscroll = function() {myFunction()};

// Get the header
var header = document.getElementById(“header”);
var hero = document.getElementById(“sticky-padding”);

// Get the offset position of the navbar
var sticky = header.offsetTop;

// Add the sticky class to the header when you reach its scroll position. Remove “sticky” when you leave the scroll position
function myFunction() {
    if (window.pageYOffset > sticky) {
        header.classList.add(“sticky”);
        hero.classList.add(“add-sticky-padding”);
    } else {
        header.classList.remove(“sticky”);
        hero.classList.remove(“add-sticky-padding”);
    }
}